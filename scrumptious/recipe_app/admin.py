from django.contrib import admin
from recipe_app.models import Recipe, RecipeStep, Ingredient

# Register your models here.
@admin.register(Recipe)
class RecipeAdmin (admin.ModelAdmin):
    list_display = [
        "title",
        "id",
        "created_on",
        "author",
    ]

@admin.register(RecipeStep)
class RecipeStep (admin.ModelAdmin):
    list_display = [
        "recipe",
        "step_number",
        "id",
        "instruction",

    ]

@admin.register(Ingredient)
class Ingredient(admin.ModelAdmin):
    list_display = [
        "recipe",
        "food_item",
        "created_date"
    ]
