from django.forms import ModelForm
from recipe_app.models import Recipe

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "rating",
        ]
